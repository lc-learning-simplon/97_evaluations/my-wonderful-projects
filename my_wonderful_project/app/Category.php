<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the projects for the category.
     */
    public function projects()
    {
        return $this->hasMany('App\Project');
    }
}
