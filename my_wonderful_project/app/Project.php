<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Get the category record associated with the project.
     */
    public function category()
    {
        return $this->hasOne('App\Category');
    }
}
