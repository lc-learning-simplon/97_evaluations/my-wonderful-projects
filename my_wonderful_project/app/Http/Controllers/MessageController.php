<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show messages list.
     *
     * @return view
     */
    public function index()
    {
        return view('messages.messages', [
            "messages" => Message::all(),
        ]);
    }

    /**
     * Show messages detail.
     *
     * @return view
     */
    public function showMessage($id)
    {
        return view('messages.show-message', [
            "message" => Message::find($id),
        ]);
    }
}
