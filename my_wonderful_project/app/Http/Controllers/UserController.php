<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show profil detail.
     *
     * @return view
     */
    public function index()
    {
        return view('users.profil', [
            "user" => User::first(),
        ]);
    }

    /**
     * Show profil update form.
     *
     * @return view
     */
    public function update()
    {
        return view('users.profil-update', [
            "user" => User::first(),
        ]);
    }

     /**
     * Submit profil update form.
     *
     * @return redirect
     */
    public function updateSubmit(Request $request)
    {
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $email = $request->input('email');
        $tel = $request->input('tel');
        $address = $request->input('address');


        $user = User::first();
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->email = $email;
        $user->tel = $tel;
        $user->address = $address;

        if ($request->file('avatar') != null) {
            $avatar = $request->file('avatar');
            $image_name = $user->id . '.' . $avatar->getClientOriginalExtension();
            $destination = public_path('/images/');
            $avatar->move($destination, $image_name);
            $path = '/images/' . $image_name;
            $user->avatar = $path;
        }

        $user->save();

        return redirect('/profil');
    }
}
