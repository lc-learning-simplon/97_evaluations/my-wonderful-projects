<?php

namespace App\Http\Controllers;

use App\User;
use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the new article form.
     *
     * @return view
     */
    public function index()
    {
        return view('articles.article-form');
    }

    /**
     * Submit the new article form.
     * Storage data in database.
     * Upload image in public/images/articles
     * 
     * @return redirect
     */
    public function submit(Request $request)
    {
        $title = $request->input('title');
        $description = $request->input('description');

        $user = User::first();
        $user_id = $user->id;

        $article = new Article();
        $article->title = $title;
        $article->description = $description;
        $article->user_id = $user_id;
        $article->save();

        $image = $request->file('image');
        $image_name = $article->id . '.' . $image->getClientOriginalExtension();
        $destination = public_path('/images/articles');
        $image->move($destination, $image_name);
        $path = '/images/articles/' . $image_name;
        $article->image_url = $path;
        $article->save();

        return redirect('/home');
    }

    /**
     * Delete the article.
     *
     * @return redirect
     */
    public function delete($id)
    {
        $article = Article::find($id);
        // In v2 delete local upload.
        $article->delete();
        return redirect('/home');
    }

    /**
     * Show the form to update the article.
     *
     * @return view
     */
    public function update($id)
    {
        return view('articles.article-update', [
            "article" => Article::find($id),
        ]);
    }

    /**
     * Submit the updated article form.
     * Storage data in database.
     * Upload image in public/images/articles
     * 
     * @return redirect
     */
    public function updateSubmit($id, Request $request)
    {
        $title = $request->input('title');
        $description = $request->input('description');


        $article = Article::find($id);
        $article->title = $title;
        $article->description = $description;
        

        if ($request->file('image') != null) {
            $image = $request->file('image');
            $image_name = $article->id . '.' . $image->getClientOriginalExtension();
            $destination = public_path('/images/articles');
            $image->move($destination, $image_name);
            $path = '/images/articles/' . $image_name;
            $article->image_url = $path;
        }

        $article->save();

        return redirect('/article/' . $id);
    }
}
