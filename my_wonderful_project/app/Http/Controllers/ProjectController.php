<?php

namespace App\Http\Controllers;

use App\Project;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProjectController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the add project form.
     *
     * @return view
     */
    public function index()
    {
        return view('projects.project-form', [
            "categories" => Category::all(),
        ]);
    }

    /**
     * Submit the new project form.
     * Storage data in database.
     * Upload image in public/images/projects
     * 
     * @return redirect
     */
    public function submit(Request $request)
    {
        $name = $request->input('name');
        $description = $request->input('description');
        $technology = $request->input('tech');
        $repo = $request->input('repo');
        $url = $request->input('url');
        $category_id = $request->input('category');

        $project = new Project();
        $project->name = $name;
        $project->description = $description;
        $project->technology = $technology;
        if ($repo != null) {
            $project->repo_url = $repo;
        }
        if ($url != null) {
            $project->website_url = $url;
        }
        $project->category_id = $category_id;
        $project->save();

        $image = $request->file('image');
        $image_name = $project->id . '.' . $image->getClientOriginalExtension();
        $destination = public_path('/images/projects/');
        $image->move($destination, $image_name);
        $path = '/images/projects/' . $image_name;
        $project->image_url = $path;
        $project->save();

        return redirect('/home');
    }

    /**
     * Delete the project.
     *
     * @return redirect
     */
    public function delete($id)
    {
        $project = Project::find($id);
        // In v2 delete local upload.
        $project->delete();
        return redirect('/home');
    }

    /**
     * Show the form to update the project.
     *
     * @return view
     */
    public function update($id)
    {
        return view('projects.project-update', [
            "project" => Project::find($id),
            "categories" => Category::all(),
        ]);
    }

    /**
     * Submit the updated project form.
     * Storage data in database.
     * Upload image in public/images/projects
     * 
     * @return redirect
     */
    public function updateSubmit($id, Request $request)
    {
        $name = $request->input('name');
        $description = $request->input('description');
        $technology = $request->input('tech');
        $repo = $request->input('repo');
        $url = $request->input('url');
        $category_id = $request->input('category');


        $project = Project::find($id);
        $project->name = $name;
        $project->description = $description;
        $project->technology = $technology;
        if ($repo != null) {
            $project->repo_url = $repo;
        }
        if ($url != null) {
            $project->website_url = $url;
        }
        $project->category_id = $category_id;
        

        if ($request->file('image') != null) {
            $image = $request->file('image');
            $image_name = $project->id . '.' . $image->getClientOriginalExtension();
            $destination = public_path('/images/articles');
            $image->move($destination, $image_name);
            $path = '/images/projects/' . $image_name;
            $project->image_url = $path;
        }

        $project->save();

        return redirect('/project/' . $id);
    }

    /**
     * Show the categories dashboard.
     *
     * @return view
     */
    public function categories()
    {
        return view('projects.categories', [
            "categories" => Category::all(),
        ]);
    }

    /**
     * Add new category on database.
     *
     * @return redirect
     */
    public function addCategory(Request $request)
    {
        $name = $request->input('name');
        $slug = $request->input('slug');


        $category = Category::where('slug', $slug)->first();
        if ($category != null) {
            return redirect('/categories');
        }
        else {
            $category = new Category();
            $category->name = $name;
            $category->slug = $slug;
            $category->save();
    
            return redirect('/categories');
        }
    }

     /**
     * Delete the category.
     *
     * @return redirect
     */
    public function deleteCategory($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect('/categories');
    }
}
