<?php

namespace App\Http\Controllers;

use App\User;
use App\Article;
use App\Category;
use App\Project;
use App\Message;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', [
            "articles" => Article::all(),
            "projects" => Project::all(),
        ]);
    }

    /**
     * Show the article informations.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showArticle($id)
    {
        return view('articles.show-article', [
            'article' => Article::find($id),
            'author'  => User::first(),
        ]);
    }

    /**
     * Show the project informations.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showProject($id)
    {
        $project = Project::find($id);
        $category_id = $project->category_id;

        return view('projects.show-project', [
            'project' => $project,
            'category'  => Category::find($category_id),
        ]);
    }

    /**
     * Submit contact message.
     *
     * @return redirect
     */
    public function contact(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $msg = $request->input('message');

        $message = new Message();
        if ($name != null) {
            $message->name = $name;
        }
        $message->email = $email;
        $message->message = $msg;
        $message->save();

        return redirect('/home');
    }
}
