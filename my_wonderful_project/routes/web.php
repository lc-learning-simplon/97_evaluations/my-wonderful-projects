<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/article/{id}', 'HomeController@showArticle');
Route::get('/project/{id}', 'HomeController@showProject');
Route::post('message/submit', 'HomeController@contact');


Route::get('/add/article', 'ArticleController@index');
Route::post('/add/article/submit', 'ArticleController@submit');
Route::post('/article/{id}/delete', 'ArticleController@delete');
Route::post('/article/{id}/update', 'ArticleController@update');
Route::post('/article/{id}/update/submit', 'ArticleController@updateSubmit');

Route::get('/categories', 'ProjectController@categories');
Route::post('/add/category', 'ProjectController@addCategory');
Route::post('/category/{id}/delete', 'ProjectController@deleteCategory');

Route::get('/add/project', 'ProjectController@index');
Route::post('/add/project/submit', 'ProjectController@submit');
Route::post('/project/{id}/delete', 'ProjectController@delete');
Route::post('/project/{id}/update', 'ProjectController@update');
Route::post('/project/{id}/update/submit', 'ProjectController@updateSubmit');

Route::get('/messages', 'MessageController@index');
Route::post('/message/{id}', 'MessageController@showMessage');

Route::get('/profil', 'UserController@index');
Route::post('/profil/update', 'UserController@update');
Route::post('/profil/update/submit', 'UserController@updateSubmit');

