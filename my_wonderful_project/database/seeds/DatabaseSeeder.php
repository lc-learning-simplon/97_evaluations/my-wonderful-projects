<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'last_name' => 'Chevallier',
            'first_name' => 'Lucas',
            'email' => 'lucaschev17@gmail.com',
            'password' => Hash::make('azerty'),
            'tel' => '0102030405',
            'address' => '5 rue des gentils'
        ]);
    }
}
