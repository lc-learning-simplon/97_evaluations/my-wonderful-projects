@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="/home">Retour</a></li>
        </ol>
    </nav>
    <h1>Modifier un projet</h1>

    <form action="/project/{{ $project->id }}/update/submit" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Nom</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $project->name }}">
        </div>
        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" class="form-control-file" id="image" name="image" accept="image/png, image/jpeg">
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3">{{ $project->description }}</textarea>
        </div>
        <div class="form-group">
            <label for="tech">Technologies</label>
            <input type="text" class="form-control" id="tech" name="tech" value="{{ $project->technology }}">
        </div>
        <div class="form-group">
            <label for="repo">Repo</label>
            <input type="text" class="form-control" id="repo" name="repo" value="{{ $project->repo_url }}">
        </div>
        <div class="form-group">
            <label for="url">Url</label>
            <input type="text" class="form-control" id="url" name="url" value="{{ $project->website_url }}">
        </div>
        <div class="form-group">
            <label for="category">Catégorie</label>
            <select class="form-control" id="category" name="category">
            @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>     
            @endforeach
            </select>
        </div>
        
         <button type="submit" class="btn btn-primary">Modifier</button>
    </form>
</div>
@endsection