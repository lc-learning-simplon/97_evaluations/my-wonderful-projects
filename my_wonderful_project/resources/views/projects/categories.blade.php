@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Mes catégories</h1>
    @foreach ($categories as $category)
    <div class="row">
        <p>{{ $category->name }}</p>
        <form action="/category/{{ $category->id }}/delete" method="POST">
            @csrf
            <button type="submit" class="btn-sm btn-danger ml-3">Supprimer</button>
        </form>
    </div>
    @endforeach

    <h2>Ajouter une catégories</h2>
    <form action="/add/category" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Nom</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="slug">Nom machine</label>
            <input type="text" class="form-control" id="slug" name="slug">
        </div>
        
        <button type="submit" class="btn btn-primary">Ajouter</button>
    </form>
</div>
@endsection