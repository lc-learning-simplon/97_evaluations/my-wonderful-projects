@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="/home">Retour</a></li>
        </ol>
    </nav>

    
    <h1>{{ $project->name }}</h1>
    <p>{{ $category->name }}</p>


    <img src="{{ $project->image_url }}" alt="{{ $project->name }}" class="img-fluid">
    <p>{{ $project->description }}</p>
    <p>{{ $project->technology }}</p>
    <a href="{{ $project->website_url }}" class="btn btn-primary" target="_blank">S'y rendre</a>
    <a href="{{ $project->repo_url }}" class="btn btn-outline-primary" target="_blank">Voir le repo</a>
</div>
@endsection