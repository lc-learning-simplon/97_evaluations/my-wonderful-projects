@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Ajouter un projet</h1>

    <form action="/add/project/submit" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Nom</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" class="form-control-file" id="image" name="image" accept="image/png, image/jpeg">
          </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="tech">Technologies</label>
            <input type="text" class="form-control" id="tech" name="tech">
        </div>
        <div class="form-group">
            <label for="repo">Repo</label>
            <input type="text" class="form-control" id="repo" name="repo">
        </div>
        <div class="form-group">
            <label for="url">Url</label>
            <input type="text" class="form-control" id="url" name="url">
        </div>
        <div class="form-group">
            <label for="category">Catégorie</label>
            <select class="form-control" id="category" name="category">
            @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>     
            @endforeach
            </select>
        </div>
        
         <button type="submit" class="btn btn-primary">Ajouter</button>
    </form>
</div>
@endsection