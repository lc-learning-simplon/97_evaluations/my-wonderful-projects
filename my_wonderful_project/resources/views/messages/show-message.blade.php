@extends('layouts.app')

@section('content')
<div class="container">
    <h2>De {{ $message->email }}</h2>
    <p>{{ $message->message }}</p>
    @if ($message->name) 
        <p>{{ $message->name }}</p>
    @endif
</div>
@endsection
