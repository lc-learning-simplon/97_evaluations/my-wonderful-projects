@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Messages</h1>
    @foreach ($messages as $message)
    <div class="row">
        <p>De {{ $message->email }}</p>
        <form action="/message/{{ $message->id }}" method="post">
            @csrf
            <button type="submit" class="btn-sm btn-primary ml-2">Lire</button>
        </form>
    </div>
    @endforeach
</div>
@endsection
