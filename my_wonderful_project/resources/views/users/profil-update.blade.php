@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="/profil">Retour</a></li>
        </ol>
    </nav>
    <h1>Modifier mon profil</h1>

    <form action="/profil/update/submit" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="first_name">Prénom</label>
            <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $user->first_name }}">
        </div>
        <div class="form-group">
            <label for="last_name">Nom</label>
            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $user->last_name }}">
        </div>
        <div class="form-group">
            <label for="avatar">Avatar</label>
            <input type="file" class="form-control-file" id="avatar" name="avatar" accept="image/png, image/jpeg">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}">
        </div>
        <div class="form-group">
            <label for="tel">Télépone</label>
            <input type="text" class="form-control" id="tel" name="tel" value="{{ $user->tel }}">
        </div>
        <div class="form-group">
            <label for="address">Adresse</label>
            <input type="text" class="form-control" id="address" name="address" value="{{ $user->address }}">
        </div>
        
        <button type="submit" class="btn btn-primary">Modifier</button>
    </form>
        

</div>
@endsection