@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1>Mon profil</h1>
            <h2>{{ $user->first_name }} {{ $user->last_name }}</h2>
            <p>{{ $user->email }}</p>
            <p>{{ $user->tel }}</p>
            <p>{{ $user->address }}</p>
            <form action="profil/update" method="post">
                @csrf
                <button type="submit" class="btn btn-primary">Modifier</button>
            </form>
        </div>
        <div class="col-6">
            <img src="{{ $user->avatar }}" alt="{{ $user->first_name }} {{ $user->last_name }}" class="img-fluid">
        </div>
    </div>

</div>
@endsection