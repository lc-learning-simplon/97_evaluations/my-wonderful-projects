@extends('layouts.app')

@section('content')
<div class="container">
    <div class="text-center">
        <h1>Accueil</h1>
        <p>( ______/ In process... \______ )</p>
    </div>
    <div>
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Projets</h2>
                <hr>
                @foreach ($projects as $project)
                <div class="row">
                    <div class="col-6">
                        <h3>{{ $project->name }}</h3>
                        <div class="row ml-1 mt-5">
                            <form action="/project/{{ $project->id }}" method="GET">
                                <button type="submit" class="btn btn-primary">Aperçu</button>
                            </form>
                            <a href="{{ $project->website_url }}" class="btn btn-outline-primary ml-3" target="_blank">S'y rendre</a>
                        </div>
                        @auth
                        <div>
                            <form action="/project/{{ $project->id }}/delete" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-danger mt-3">Supprimer</button>
                            </form>
                            <form action="/project/{{ $project->id }}/update" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-primary mt-3">Modifier</button>
                            </form>
                        </div>
                        @endauth
                    </div>
                    <div class="col-6">
                        <img src="{{ $project->image_url }}" alt="{{ $project->name }}" class="img-fluid">
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div>
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Articles</h2>
                <hr>
                @foreach ($articles as $article)
                <div class="row">
                    <div class="col-6">
                        <img src="{{ $article->image_url }}" alt="{{ $article->title }}" class="img-fluid">
                    </div>
                    <div class="col-6 text-right">
                        <h3>{{ $article->title }}</h3>
                        <form action="/article/{{ $article->id }}" method="GET">
                            <button type="submit" class="btn btn-primary mt-5">En savoir plus</button>
                        </form>
                        @auth
                        <div class="text-right">
                            <form action="/article/{{ $article->id }}/delete" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-danger mt-3">Supprimer</button>
                            </form>
                            <form action="/article/{{ $article->id }}/update" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-primary mt-3">Modifier</button>
                            </form>
                        </div>
                        @endauth
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div>
        <br>
        <h2>Contact</h2>
        <form action="message/submit" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nom</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" id="message" name="message" rows="3"></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Envoyer</button>
        </form>
    </div>
    
</div>
@endsection
