@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="/home">Retour</a></li>
        </ol>
    </nav>

    <h1>{{ $article->title }}</h1>

    <img src="{{ $article->image_url }}" alt="{{ $article->title }}" class="img-fluid">
    <p>{{ $article->description }}</p>
    <div class="row">
        <p class="d-flex align-items-center ml-3">Par {{ $author->first_name }} {{ $author->last_name }}</p>
        <img src="{{ $author->avatar }}" alt="{{ $author->first_name }} {{ $author->last_name }}" class="img-fluid col-1">
    </div>
</div>
@endsection