@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Ajouter un article</h1>

    <form action="/add/article/submit" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">Titre</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" class="form-control-file" id="image" name="image" accept="image/png, image/jpeg">
          </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
        </div>
        
         <button type="submit" class="btn btn-primary">Ajouter</button>
    </form>
</div>
@endsection