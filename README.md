# My Wonderful projects

## Quickstart

1. Clone the project.

2. `cd my_wonderful_project`

3. `composer install`

4. `npm install && npm run dev`

5. `touch .env`, paste the .env.exemple content add add your database configuration.

6. `php artisan migrate`

7. `php artisan db:seed` to add the user. To access on backoffice use the email and password in the seed `database/seeds/DatabaseSeeder.php`

8. `php artisan serve` to launch the server.

## Technologies

- Framwork PHP Laravel.
- Laravel UI.
- Frameworl CSS Boostsrap.
- Database on MySql.
- Local Development.

## Exernal links

[Kanban](https://tree.taiga.io/project/lucaschev17-elavluation-4-my-wonderful-projects/kanban?kanban-status=2187018)