# Carnet de bord.

**Jour 1**
- Création d'un kanban.
- Création du dépôt gitlab.
- Ajout du readme, des consignes et du carnet de bord.
- Installation de Laravel.
- Installation de Laravel UI.
- Réalisation d'un schéma de la base de donnée.
- Création de la base de donnée my_wonderful_projects en local avec MySql.
- Création d'un utilisateur pour l'utilisation de cette base de données.
- Mise en place des migrations conforme à mon schéma de base de données.
- Définition du defaultStringLength.
- Mise en place d'un seed utilisateur.
- Rédaction du quickstart.
- Création des wireframes sur whimsical.com.
- Mise en place de l'interface de connexion et déconnexion avec redirection sur /home.

**Jour 2**
- Réalisation de la navbar.
- Réalisation du squelette de la page d'accueil.
- Développement des users stories articles.

**Jour 3**
- Mise en place d'un léger front avec bootstrap pour se rapporcher de la maquette pour article.
- Développement des users stories catégories.
- Développement des users stories projets.
- Développement des users stories messages.
- Développement du profil utilisateur et de la modification. Pas de modification de mot de passe pour le moment.

