# My wonderful projects

## Conseil
Prioriser vos tâches, en faite qu'une à la fois (petite victoire).

Une fois la tâche terminer faite commit (important), il y à environ une trentaine de user-stories donc 30 commit.

Faites ce que vous savez faire.

Allez au plus simple, si c'est beaucoups trop compliqué ce n'est pas la bonne solution.

Oublier pas le cheval !

Vous en êtes capable !

## Arborécense

Vous êtes libre de faire ça sur une page ou plusieurs tant qu'on à accéder aux dernier projets poster, article et un formulaire de contact.

## Data
Votre BDD doit contenir 4 tables (au minimum) :
- users :
  - id
  - last_name | nullable
  - first_name | nullable
  - email
  - password 
  - avatar : nullable|default_value
  - tel : nullable
  - address : nullable
- projets :
  - id
  - name
  - description
  - image_url
  - techonology
  - repo_url : nullable
  - website_url : nullable
  - category_id 
- articles :
  - id
  - title
  - description
  - image_url
  - user_id
- categories :
  - id
  - slug
  - name
- messages :
  - id
  - name : optional
  - email
  - message
  
## Users stories

> Vous pouvez en rajouter de nouvelle dans votre KANBAN si c'est utile pour vous.

**En tant qu'utilisateur je veux :**
  - Pouvoir accéder me connecter afin de accéder à mon back-office
  - Pouvoir accéder me déconnecter afin de clôturer ma session
____
  - Pouvoir accéder à la liste de mes Projets afin de pouvoir avoir un appercus, les éditer, supprimer ou en créer
  - Pouvoir créer un nouveau Projet afin de les mettre en avant sur mon portfolio
  - Pouvoir assigner une catégorie au projet afin de pouvoir les trier.
  - Pouvoir éditer un Projet afin de mettre à jours ces informations
  - Pouvoir accéder à un apercue de mon Projet afin de pouvoir contempler la beauté de celui-ci
  - Pouvoir supprimer un Projet afin de le retirer de la liste des projets présent de mon portfolio
____
  - Pouvoir accéder à la liste de mes Articles afin de pouvoir avoir un appercue, les éditer, supprimer ou en créer
  - Pouvoir créer un nouvel Article afin de les mettre en avant sur mon portfolio
  - Pouvoir éditer un Article afin de mettre à jours ces informations
  - Pouvoir mettre un Article en brouillon afin de reprendre la rédaction plus tard 
  - Pouvoir accéder à un apercue de mon Article afin de pouvoir contempler la beauté de celui-ci
  - Pouvoir supprimer un Article afin de le retirer de la liste des articles présent de mon portfolio
___
  - Pouvoir accéder à la liste de mes Catégories afin de pouvoir avoir un appercue, les éditer, supprimer ou en créer
  - Pouvoir créer une nouvel Catégorie afin de les mettre en avant sur mon portfolio
  - Pouvoir éditer une Catégorie afin de mettre à jours ces informations
___
  - Pouvoir accéder à la liste des Messages envoyer par les visiteur du site
  - Pouvoir différencier un Message lu d'un message non-lue
  - Pouvoir accéder à un Message afin de qui en est l'expéditeur et lire le message
  - Pouvoir supprimer le Message afin de le retirer de la liste des messages.
___
  - Pouvoir accéder à mes informations personnel afin de les compléter ou les éditer
  - Pouvoir accéder à ma page d'acceuil || aux différentes pages afin de consulter les derniers projets, articles.
___
___
**En tant que visiteur je veux :**
  - Pouvoir accéder à la liste des Articles
  - Pouvoir accéder à un apercue d'un Article afin de lire celui-ci
  - Pouvoir accéder à la liste des Projets
  - Pouvoir accéder à un apercue d'un Projet afin d'avoir des details celui-ci
  - Pouvoir accéder à la version en ligne du projet afin d'avoir un apercue du travail effectuer
  - Pouvoir accéder à la repertoire du projet afin d'avoir un apercue de la technique utiliser
  - Pouvoir accéder aux téchnologies utilisé par ce projet afin d'avoir un apercue des téchnologie pour celui-ci
  - Pouvoir envoyer un message à au propritaire du site afin de lui faire parvenir une information.
  
## And Voilà